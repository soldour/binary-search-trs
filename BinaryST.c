#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct node
{

int data;
struct	node* left;
struct node* right;

};

struct node* creatNewNode(int data) {
	struct node* newNode = malloc(sizeof(struct node));
	newNode->data = data;
	newNode->left = NULL;
	newNode->right = NULL;
	return newNode;

}

struct node * insertNewNode(struct node * parent, int value) {
	if (parent == NULL) {
		parent = creatNewNode(value);

		return parent;
	}
	else if (value <= parent->data) {
		parent->left = insertNewNode(parent->left, value);

	}else{

		parent->right = insertNewNode(parent->right, value);
	}
	return parent;
}




bool islessOrGreater(struct node* parent, int data) {
	if (data<=parent->data ) return true;

	else return false;

}





bool traverseThrough(struct node* parent, int data) {

	while(parent != NULL) {

		bool isTrue = islessOrGreater(parent, data);
		if (isTrue) parent = parent->left;
		else if (!isTrue) parent = parent->right;
		else return  printf("\nThe element %d is found !", data); true;
	
	}
      printf("\nThe element was not found or tree is empity!"); 
      return false;
}
		


	  

bool search(struct node* parent, int data) {

	if (parent == NULL) {
		return false;

	}
	else if (parent->data == data) {
		printf("The element %d is found at the root", data);
		return true;
	}
	else if (data <= parent->data) {
		search(parent->left, data);
		return true;
	}
	else {
		search(parent->right, data);
		printf("\nThe element %d is found on the right tree", data);
		return true;
	}
}



struct node* FindMin(struct node* parent)
{
	while (parent->left != NULL) parent = parent->left;
	return parent;
}


struct node * deleteNode(struct node* parent, int data) {
	if (parent == NULL) return parent;
	else if (data < parent->data) parent->left = deleteNode(parent->left, data);
	else if (data > parent->data) parent->right = deleteNode(parent->right, data);
		
	else {
		
		if (parent->left == NULL && parent->right == NULL) {
			free( parent);
			printf("\nthe element %d is deleted ", data);
			parent = NULL;
		}
		
		else if (parent->left == NULL) {
			struct Node* temp = parent;
			parent = parent->right;
			free (temp);
			printf("\nthe element %d is deleted ", data);
		}
		else if (parent->right == NULL) {
			struct node* temp = parent;
			parent = parent->left;
			free( temp);
			printf("\nthe element %d is deleted ", data);
		}
	
		else {
			struct node* temp = FindMin(parent->right);
			parent->data = temp->data;
			parent->right = deleteNode(parent->right, temp->data);
			printf("\nthe element %d is deleted ", data);
		}
	}
	return parent;
}
		
		



int main() {
	struct node* head = NULL;

	head= insertNewNode(head, 40);
	head = insertNewNode(head, 100);
	head = insertNewNode(head, 10);
	head = insertNewNode(head, 120);
	head = insertNewNode(head, 80);
	head = insertNewNode(head, 90);
	head = insertNewNode(head, 75);
	head = insertNewNode(head, 63);

	traverseThrough(head,500);
	deleteNode(head, 75);
	traverseThrough(head, 75);
	

}
